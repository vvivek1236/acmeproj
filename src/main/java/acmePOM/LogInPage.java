package acmePOM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LogInPage extends ChromeDriver1{

	public LogInPage()  {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="email") WebElement eleUserName;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLogInButton;
	
	public LogInPage userName() {
		clearAndType(eleUserName, "vivekany@gmail.com");
		return new LogInPage();
	}
	
	public LogInPage passWord() {
		clearAndType(elePassword, "v.vivek1236");
		return new LogInPage();
	}
	public DashBoardPage click() {
		click(eleLogInButton);
		return new DashBoardPage();
	}
}
