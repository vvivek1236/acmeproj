package acmePOM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class VendorSearchResult extends ChromeDriver1 {
	
	public VendorSearchResult() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH,using ="(//tr//td)[1]") WebElement eleVendorName;
	public void result() {
		String venName = eleVendorName.getText();
		System.out.println(venName);
	}
}
