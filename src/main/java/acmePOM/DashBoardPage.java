package acmePOM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DashBoardPage extends ChromeDriver1{
	
	public DashBoardPage() {
		PageFactory.initElements(driver,this);
	}
	@FindBy(how = How.XPATH,using ="//i[@class='fa fa-truck']") WebElement vendors;
	@FindBy(how = How.LINK_TEXT,using= "Search for Vendor") WebElement eleClick;

	public DashBoardPage mouseOver() {
		Actions action = new Actions(driver);
		action.moveToElement(vendors).pause(3000).perform();
		return new DashBoardPage();
	}

	public VendorPage click() {
		click(eleClick);
		return new VendorPage();
	}

}
