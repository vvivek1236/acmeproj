package acmePOM;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

public class ChromeDriver1 {
	static ChromeDriver driver;
	@BeforeMethod
	public void chDriver() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
	}
	
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
		} catch (InvalidElementStateException e) {
		} catch (WebDriverException e) {
		
		}
	}
	
	public void click(WebElement ele) {
		String text = "";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));			
			text = ele.getText();
			ele.click();
			//logStep("The element "+text+" is clicked", "pass");
		} catch (InvalidElementStateException e) {
			//logStep("The element: "+text+" could not be clicked", "fail");
		} catch (WebDriverException e) {
			//logStep("Unknown exception occured while clicking in the field :", "fail");
		} 

	}
}