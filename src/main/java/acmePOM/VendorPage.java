package acmePOM;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class VendorPage extends ChromeDriver1{

	public VendorPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using = "vendorTaxID") WebElement eleVendor;
	@FindBy(how = How.ID,using = "buttonSearch") WebElement eleButton;
	
	public VendorPage vendorSearch() {
		clearAndType(eleVendor, "DE767565");
		return new VendorPage();
	}
	
	public VendorSearchResult clickSearch() {
		click(eleButton);
		return new VendorSearchResult();
	}

}
